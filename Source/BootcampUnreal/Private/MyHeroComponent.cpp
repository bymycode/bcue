#include "MyHeroComponent.h"
#include "Components/CapsuleComponent.h"
#include <Runtime\Engine\Public\DrawDebugHelpers.h>

UMyHeroComponent::UMyHeroComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UMyHeroComponent::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("Warning: Begin Play"));
	GetOwner()->InputComponent->BindAction("ReportLocation", IE_Pressed, this, &UMyHeroComponent::LogPosition);
	GetOwner()->InputComponent->BindAction("Grab", IE_Pressed, this, &UMyHeroComponent::GrabAction);
	GetOwner()->InputComponent->BindAction("Release", IE_Pressed, this, &UMyHeroComponent::DropAction);
	UCapsuleComponent* CapsuleComp = static_cast<UCapsuleComponent*>(GetOwner()->GetRootComponent());
	CapsuleComp->OnComponentHit.AddDynamic(this, &UMyHeroComponent::OnCompHit);

	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	PhysicsHandle->bRotationConstrained = true;
}


void UMyHeroComponent::LogPosition()
{
	FString PosStr = GetOwner()->GetActorLocation().ToString();
	UE_LOG(LogTemp, Warning, TEXT("Hero pos: %s"), *PosStr);
}

void UMyHeroComponent::OnCompHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor != nullptr)
	{
		FString OtherName = OtherActor->GetName();
		if (OtherName != LastBumpedInto)
		{
			FString DisplayText = FString::Printf(TEXT("Bumped into: %s"), *OtherName);
			//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, DisplayText);
			LastBumpedInto = OtherName;
		}
	}
}

void UMyHeroComponent::GrabAction()
{
	if (PhysicsHandle->GetGrabbedComponent())
	{
		return;
	}

	FVector OutPlayerViewPointLocation;
	FRotator OutPlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OutPlayerViewPointLocation, OutPlayerViewPointRotation);

	FVector LineStart = GetOwner()->GetActorLocation();
	FVector Forward = GetOwner()->GetActorForwardVector();
	FVector Look = OutPlayerViewPointRotation.Vector();
	Forward.Z = Look.Z;

	FVector LineEnd = LineStart + Forward * 150;

	DrawDebugLine(GetWorld(), LineStart, LineEnd, FColor(255, 0, 0), false, 5.0f, 0, 5);

	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());
	
	FHitResult HitResult;
	GetWorld()->LineTraceSingleByObjectType(
		HitResult,
		LineStart,
		LineEnd,
		FCollisionObjectQueryParams(
			ECollisionChannel::ECC_PhysicsBody),
		TraceParameters);

	if (HitResult.bBlockingHit)
	{
		AActor* TargetActor = HitResult.GetActor();
		UE_LOG(LogTemp, Warning, TEXT("Hit Result: %s"), *HitResult.ToString());

		FString ActorName = TargetActor->GetName();
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Cyan, FString::Printf(TEXT("Grabbing \'%s\'..."), *ActorName));

		UPrimitiveComponent* Comp = HitResult.GetComponent();
		UE_LOG(LogTemp, Warning, TEXT("Component: %s"), *Comp->GetName());
		Comp->SetSimulatePhysics(false);
		Comp->GetOwner()->SetActorScale3D(FVector(0.75f));

		PhysicsHandle->GrabComponent(Comp, TEXT("head"), FVector::ZeroVector, false);
	}
}

void UMyHeroComponent::DropAction()
{
	UPrimitiveComponent* Comp = PhysicsHandle->GetGrabbedComponent();
	if (Comp)
	{
		Comp->GetOwner()->SetActorScale3D(FVector(1.0f));
		Comp->SetSimulatePhysics(true);
		PhysicsHandle->ReleaseComponent();
	}	UE_LOG(LogTemp, Warning, TEXT("DropAction"));
}

void UMyHeroComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	UPrimitiveComponent* GrabbedComponent = PhysicsHandle->GetGrabbedComponent();
	if (GrabbedComponent)
	{
		FVector Pos = GetOwner()->GetActorLocation() + GetOwner()->GetActorForwardVector() * 100;
		Pos.Z += 50.0f;
		GrabbedComponent->GetOwner()->SetActorLocation(Pos);
	}
}
