#include "MyHeroComponent2.h"

// Sets default values for this component's properties
UMyHeroComponent2::UMyHeroComponent2()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UMyHeroComponent2::BeginPlay()
{
	Super::BeginPlay();
}


// Called every frame
void UMyHeroComponent2::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (!TargetActor)
	{
		return;
	}

	FVector TargetEyeDir = TargetActor->GetActorForwardVector();
	FVector HeroEyeDir = GetOwner()->GetActorForwardVector();
	
	
	float Angle = FMath::RadiansToDegrees(
		FMath::Atan2(
			FVector::CrossProduct(HeroEyeDir, TargetEyeDir).Size(),
			FVector::DotProduct(HeroEyeDir, TargetEyeDir)));

	if (Angle < 10)
	{
		if (!bMsgShown)
		{
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Red,
				TEXT("The Gizmo and me are looking at the same direction!"));
			UE_LOG(LogTemp, Warning, TEXT("Angle: %f"), Angle);
			bMsgShown = true;
		}
	}
	else
	{
		bMsgShown = false;
	}
}

