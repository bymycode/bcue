// Fill out your copyright notice in the Description page of Project Settings.


#include "MyDebugDrawer.h"
#include <Runtime\Engine\Public\DrawDebugHelpers.h>

// Sets default values for this component's properties
UMyDebugDrawer::UMyDebugDrawer()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMyDebugDrawer::BeginPlay()
{
	Super::BeginPlay();
	FVector StartLine = GetOwner()->GetActorLocation();
	FVector EndLineX = GetOwner()->GetActorForwardVector();
	FVector EndLineY = GetOwner()->GetActorRightVector();
	FVector EndLineZ = FVector::CrossProduct(EndLineX, EndLineY);

	UE_LOG(LogTemp, Warning,
		TEXT("UMyDebugDrawer::BeginPlay(): Start: %s; Cross Product: %s"), *StartLine.ToString(), *EndLineZ.ToString());

	const float VecLen = 500.0f;
	DrawDebugLine(GetWorld(), StartLine, StartLine + EndLineX * VecLen, FColor(255, 0, 0), true, -1, 0, 5);
	DrawDebugLine(GetWorld(), StartLine, StartLine + EndLineY * VecLen, FColor(0, 255, 0), true, -1, 0, 5);
	DrawDebugLine(GetWorld(), StartLine, StartLine + EndLineZ * VecLen, FColor(0, 0, 255), true, -1, 0, 5);

	DrawDebugLine(GetWorld(), StartLine, StartLine + (EndLineX + EndLineY + EndLineZ).GetSafeNormal() * VecLen, FColor(255, 255, 255), true, -1, 0, 5);
	
}


// Called every frame
void UMyDebugDrawer::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//UE_LOG(LogTemp, Warning, TEXT("UMyDebugDrawer::TickComponent()"));
}

